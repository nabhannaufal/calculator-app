const displayValue = document.getElementById("value");
let tempValue = "";

const displayResult = () => {
  displayValue.innerHTML = tempValue;
};

const onDelete = () => {
  if (tempValue == "Syntax Error" || tempValue == "Infinity" || tempValue == "NaN") {
    tempValue = "";
  }
  tempValue = tempValue.slice(0, -1);
  displayResult();
};
const onReset = () => {
  tempValue = "";
  displayResult();
};

const onCalc = () => {
  try {
    tempValue = eval(tempValue).toString();
  } catch (error) {
    tempValue = "Syntax Error";
  }

  displayResult();
};

const onClickButton = (value) => {
  if (tempValue === "Syntax Error") tempValue = "";
  tempValue = tempValue.concat(value);
  displayResult();
};
