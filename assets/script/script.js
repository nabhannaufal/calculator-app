const defaultTheme = Number(localStorage.getItem("theme"));
const roundToggle = document.getElementById("roundToggle");

const root = document.querySelector(":root");

const theme1 = {
  "--bg": "#3b4564",
  "--text": "#fdfffd",
  "--cover": " #181e33",
  "--coverButton": "#242d44",
  "--button": "#ebe2dc",
  "--buttonText": "#444a56",
  "--buttonShadow": "#b5a59b",
  "--buttonPrimary": "#657199",
  "--buttonPrimaryText": "#ffffff",
  "--buttonPrimaryShadow": "#404d75",
  "--buttonSecondary": "#d04031",
  "--buttonSecondaryText": "#ffebec",
  "--buttonSecondaryShadow": "#902315",
};
const theme2 = {
  "--bg": "#E6E6E6",
  "--text": "#323229",
  "--cover": " #EEEEEE",
  "--coverButton": "#D3CDCD",
  "--button": "#E5E4E1",
  "--buttonText": "#383830",
  "--buttonShadow": "#A99D91",
  "--buttonPrimary": "#398185",
  "--buttonPrimaryText": "#F6FDFD",
  "--buttonPrimaryShadow": "#17575B",
  "--buttonSecondary": "#C35200",
  "--buttonSecondaryText": "#FFF9E3",
  "--buttonSecondaryShadow": "#7B3402",
};
const theme3 = {
  "--bg": "#17052A",
  "--text": "#FFED43",
  "--cover": " #1E0936",
  "--coverButton": "#1E0936",
  "--button": "#331B50",
  "--buttonText": "#FFE93E",
  "--buttonShadow": "#831D9B",
  "--buttonPrimary": "#57077C",
  "--buttonPrimaryText": "#FFFAFF",
  "--buttonPrimaryShadow": "#BF16F6",
  "--buttonSecondary": "#00DECF",
  "--buttonSecondaryText": "#003134",
  "--buttonSecondaryShadow": "#6DF7EF",
};

const applyTheme = (theme) => Object.keys(theme).forEach((key) => root.style.setProperty(key, theme[key]));

const setTheme = (theme = 1) => {
  if (theme === 1) {
    roundToggle.style.transform = "translateX(0)";
    applyTheme(theme1);
    localStorage.setItem("theme", 1);
  }
  if (theme === 2) {
    roundToggle.style.transform = "translateX(1.2rem)";
    applyTheme(theme2);
    localStorage.setItem("theme", 2);
  }
  if (theme === 3) {
    roundToggle.style.transform = "translateX(2.4rem)";
    applyTheme(theme3);
    localStorage.setItem("theme", 3);
  }
};

setTheme(defaultTheme);
